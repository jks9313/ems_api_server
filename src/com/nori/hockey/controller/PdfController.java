package com.nori.hockey.controller;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nori.Core.utility.StringUtil;
import com.nori.hockey.domain.Pdf;
import com.nori.hockey.persistence.mysql.mybatis.mapper.PdfMapper;
import com.nori.hockey.utility.ConversionPDF;

@Controller
@RequestMapping(value = "/")
public class PdfController {
	
	@Autowired private PdfMapper pm;
	
	@RequestMapping(value = "/pdf")
	public String boardList(@RequestParam(value="id") int id,HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		Pdf list = pm.selectListAll(id);
		model.addAttribute("pdf",list);
		return "pdf";
	}
	@ResponseBody
	@RequestMapping(value = "/convertPDF" )
	public String convertPDF(@RequestParam("name") String name,@RequestParam("date") String date, HttpServletRequest request, HttpServletResponse response, @RequestParam(value="html") String html, ModelMap modelMap) throws Exception {
		ConversionPDF conversionPDF	=	new ConversionPDF();
		
		String pdfPath = conversionPDF.createPdf(html ,response,name,date);
		
		return pdfPath;
	}
	
}
