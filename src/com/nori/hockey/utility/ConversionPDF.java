package com.nori.hockey.utility;



import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.CssAppliers;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.nori.hockey.file.S3Service;

public class ConversionPDF {

	//local 서버
//	private static final String uploadpath	="upload";
//	private static final String fileUploadPath = "/Users/jeong-gyeongsu/STSWork/PDFSERVER/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/webapps/";
//	private String dailyRecordPDF ="dailyRecord.pdf";
//	private String dailyRecordImage ="dailyRecord.jpeg";
//	private static final String pdfSavingPath ="/savingPDF";
//	private static final String imgSavingPath ="/savingPdfByImage";
//	private static final String resourcePath ="/Users/jeong-gyeongsu/STSWork/PDFSERVER/PDF/web";
	
	//aws 서버
	private static final String uploadpath	="upload";
	private static final String fileUploadPath = "/var/lib/tomcat8/webapps/";
	private String dailyRecordPDF ="개인정보동의.pdf";
	private String dailyRecordImage ="dailyRecord.jpeg";
	private static final String pdfSavingPath ="/savingPDF";
	private static final String imgSavingPath ="/savingPdfByImage";
	private static final String resourcePath ="/var/lib/tomcat8/webapps/ROOT";

public String createPdf(String dailyRecord, HttpServletResponse response,String name, String date)
    throws DocumentException, IOException {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar c1 = Calendar.getInstance();
        String strToday  = sdf.format(c1.getTime());
        
        //aws
        dailyRecordPDF	=	date+"_"+name;
        
        
        int idx = dailyRecordPDF.indexOf(File.separator); 
        
      
        //aws
        String fileNm = uploadpath+pdfSavingPath+File.separator+dailyRecordPDF+".pdf";
        
      
        //aws
        String serverUploadPath = fileUploadPath+uploadpath+pdfSavingPath+File.separator+dailyRecordPDF;
        
        Document document = new Document(PageSize.B4, 55, 0, 20, 20);     
        XMLWorkerHelper xmlWorkerHelper = XMLWorkerHelper.getInstance();
        PdfWriter writer	= PdfWriter.getInstance(document, new FileOutputStream(serverUploadPath));
        writer.setInitialLeading(12.5f);
        document.open();
        
        //CSS FILE
        CSSResolver cssResolver = new StyleAttrCSSResolver();
//        CssFile cssFile1 = xmlWorkerHelper.getCSS(new FileInputStream(resourcePath + "/coreui/vendors/@coreui/icons/css/coreui-icons.min.css"));
//        CssFile cssFile2 = xmlWorkerHelper.getCSS(new FileInputStream(resourcePath + "/coreui/vendors/flag-icon-css/css/flag-icon.min.css"));
//        CssFile cssFile3 = xmlWorkerHelper.getCSS(new FileInputStream(resourcePath + "/coreui/vendors/font-awesome/css/font-awesome.min.css"));
//        CssFile cssFile4 = xmlWorkerHelper.getCSS(new FileInputStream(resourcePath + "/coreui/vendors/simple-line-icons/css/simple-line-icons.css"));
//        cssResolver.addCss(cssFile1);
//        cssResolver.addCss(cssFile2);
//        cssResolver.addCss(cssFile3);
//        cssResolver.addCss(cssFile4);
        
        
     // HTML, 폰트 설정
        XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
        fontProvider.register(resourcePath+"/NanumGothic.ttf", "Nanum"); // MalgunGothic은 alias,
        CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
         
        HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
        
        PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
        HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
        
        XMLWorker worker = new XMLWorker(css, true);
        XMLParser xmlParser = new XMLParser(worker, Charset.forName("UTF-8"));
        
        StringReader strReader = new StringReader(dailyRecord);
        System.out.println(strReader);
        
        
        
        xmlParser.parse(strReader);
         
        document.close();
        writer.close();
        S3Service s3Service	= new S3Service();
        File pdfFile = new File(serverUploadPath);
        FileInputStream input = new FileInputStream(pdfFile);
        MultipartFile multiPdfFile = new MockMultipartFile(dailyRecordPDF, input);
        String pdfPath = s3Service.upload(multiPdfFile, fileUploadPath,fileNm);
     
        //pdf파일 image 처리후 저장
       
        
        
        return pdfPath;
        
    }
    
    public void downloadDailyRecordPDF(HttpServletResponse response, String serverUploadPath, String fileNm) {
    	FileInputStream fis = null;
    	ServletOutputStream out = null;

    		try {
    			out = response.getOutputStream();
    			
    	
    			File file = new File(serverUploadPath);
    	
    			if (!file.exists()) {
    				String str = "<script>alert('저장된 파일이 없습니다.'); history.back(-1);</script>";
    				response.setContentType("text/html; charset=UTF-8");
    				response.getOutputStream().write(str.getBytes());
    				return;
    			}
    	
    			fis = new FileInputStream(file);
    			System.out.println(fileNm);
    			response.setHeader("Content-Type", "application/octet-stream; charset=utf-8");
    			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNm + "\";");
    			response.setHeader("Content-Transfer-Encoding", "binary;");
    	
    			response.setHeader("Pragma", "no-cache;");
    			response.setHeader("Expires", "-1;");
    	
    			int byteRead = 0;
    			byte[] buffer = new byte[8192];
    			while ((byteRead = fis.read(buffer, 0, 8192)) != -1) {
    				out.write(buffer, 0, byteRead);
    			}
    			out.flush();
    		} catch (Exception e) {
    			// TODO: handle exception
    			throw new RuntimeException(e);
    		} finally {
    			if (out != null)
    				try {
    					out.close();
    				} catch (IOException e) {
    				}
    			if (fis != null)
    				try {
    					fis.close();
    				} catch (IOException e) {
    				}
    		}
    }
	
	
	
}